import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import Table from 'antd/lib/table'
import Tooltip from 'antd/lib/tooltip'
import Button from 'antd/lib/button'
import Icon from 'antd/lib/icon'
import { Link } from 'react-router-dom'

import { deleteMerchant } from '../actions/actions'

class MerchantList extends Component {
  constructor (props) {
    super(props)
    this.columns = [
      {
        title: 'Avatar',
        dataIndex: 'avatarUrl',
        render: (val, row) =>  val 
          ? <img style={{ maxWidth: 30, maxHeight: 30 }} src={val} alt={`${row.firstname}'s profile pictute`} />
          : <Icon style={{ fontSize: 30 }} type='user' />
      },
      {
        title: 'Name',
        dataIndex: '',
        render: (v, row) => `${row.firstname} ${row.lastname}`
      },
      {
        title: 'Email',
        dataIndex: 'email'
      },
      {
        title: 'Phone',
        dataIndex: 'phone'
      },
      {
        title: 'Premium',
        dataIndex: 'hasPremium',
        render: (val) => String(val)
      },
      {
        title: 'Bids',
        dataIndex: 'bids',
        render: (val) => val.length
      },
      {
        title: 'Actions',
        render: (v, merchant) => (
          <div>
            <Link to ={`/merchant/${merchant.id}/`}>
              <Tooltip title='View'>
                <Button shape='circle' icon='eye' />
              </Tooltip>
            </Link>
            <Link to ={`/merchant/edit/${merchant.id}`}>
              <Tooltip title='Edit'>
                <Button shape='circle' icon='edit' />
              </Tooltip>
            </Link>
            <Tooltip title='Delete' onClick={() => this.props.dispatch(deleteMerchant(merchant.id))} >
              <Button shape='circle' icon='close' />
            </Tooltip>
          </div>
        )
      },
    ]
  }

  render () {
    const { merchants } = this.props

    return (
      <div>
        <h2> Merchants </h2>
        <Link to='/merchant/create'>
          Create new merchant
        </Link>
        <Table
          rowKey={(record) => record.id}
          columns={this.columns}
          dataSource={merchants}
        />
      </div>
    )
  }
}

MerchantList.propTypes = {
  merchants: PropTypes.array.isRequired
}

const mapStateToProps = state => {
  return {
    ...state
  }
}

export default connect(mapStateToProps)(MerchantList)