import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import Table from 'antd/lib/table'
import Card from 'antd/lib/card'
import { Link } from 'react-router-dom'

const dataDisplay = (label, value) => <div> <b> {label}: </b> {value} </div>

class MerchantView extends Component {
  constructor (props) {
    super(props)
    this.columns = [
      {
        title: 'Car',
        dataIndex: 'carTitle',
        sorter: (a, b) => {
          if(a.carTitle < b.carTitle) return -1
          if(a.carTitle > b.carTitle) return 1
          return 0
        }
      },
      {
        title: 'Amount',
        dataIndex: 'amount',
        sorter: (a, b) => a.amount - b.amount
      },
      {
        title: 'Date',
        dataIndex: 'created',
        sorter: (a, b) => {
          if (new Date(a.created) < new Date(b.created)) return -1
          if (new Date(a.created) > new Date(b.created)) return 1
          return 0
        }
      }
    ]

    this.id = this.props.match.params.id
    this.state = this.props.merchants.find(merchant => merchant.id === this.id)
  }

  render () {
    return (
      <div>
        <Card
          title={`${this.state.firstname} ${this.state.lastname} merchant's data:`}
          style={{ marginBottom: '20px' }}
          extra={<Link to={`/merchant/edit/${this.id}`}> Edit merchant </Link>}
        >
          {this.state.avatarUrl
            ? <img
              src={this.state.avatarUrl}
              style={{ float: 'left', maxWidth: '30%', maxHeight: 60 }}
              alt={`${this.state.firstname}'s profile pictute`}
            />
            : null
          }

          {dataDisplay('Email', this.state.email)}
          {dataDisplay('Phone', this.state.phone)}
          {dataDisplay('Premium', this.state.hasPremium ? 'Yes' : 'No')}
        </Card>
        <Table
          rowKey={(record) => record.id}
          columns={this.columns}
          dataSource={this.state.bids}
        />
      </div>
    )
  }
}

MerchantView.propTypes = {
  merchants: PropTypes.array.isRequired,
  dispatch: PropTypes.func.isRequired
}

const mapStateToProps = state => {
  return {
    ...state
  }
}

export default connect(mapStateToProps)(MerchantView)