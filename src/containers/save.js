import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import Form from 'antd/lib/form'
import Input from 'antd/lib/input'
import Switch from 'antd/lib/switch'
import Row from 'antd/lib/row'
import Col from 'antd/lib/col'
import Button from 'antd/lib/button'

import { create, edit } from '../actions/actions'

const FormItem = Form.Item

const InputContainer = (label, value, onChange, type = 'text') => (
  <Col xs={22} md={11}>
    <FormItem label={label} required>
      <Input type={type} value={value} onChange={onChange} />
    </FormItem>
  </Col>
)

InputContainer.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  type: PropTypes.string
}

class MerchantSave extends Component {
  constructor (props) {
    super(props)

    this.id = this.props.match.params.id
    this.mode = this.id ? 'edit' : 'create'
    this.state = this.props.merchants.find(merchant => merchant.id === this.id) || {}
  }

  _submit (e) {
    if (e) e.preventDefault()
    let data = {
      ...this.state
    }

    // bids cant be created/edited here, so no reason to send it
    delete data.bids

    // todo: validate if all required fields are filled
    if (this.mode === 'edit') {
      this.props.dispatch(edit(data))
    } else {
      this.props.dispatch(create(data))
    }
    // todo: only redirect after success
    this.props.history.push('/')
  }

  render () {
    return (
      <div>
        <Form onSubmit={(e) => this._submit(e)} >
          <Row type='flex' justify='space-between' >
            {InputContainer('First Name', this.state.firstname, (e) => this.setState({ firstname: e.target.value}))}
            {InputContainer('Last Name', this.state.lastname, (e) => this.setState({lastname: e.target.value}))}
            {InputContainer('Email', this.state.email, (e) => this.setState({email: e.target.value}), 'email')}
            {InputContainer('Avatar URL', this.state.avatarUrl, (e) => this.setState({avatarUrl: e.target.value}))}
            {InputContainer('Phone', this.state.phone, (e) => this.setState({phone: e.target.value}))}
            <Col xs={22} md={11}>
              <FormItem label='Premium user' required>
                <Switch checked={this.state.hasPremium} onChange={e => this.setState({hasPremium: !this.state.hasPremium})} />
              </FormItem>
            </Col>
          </Row>
          <Row type='flex' justify='end' style={{ marginBottom: '15px' }}>
            <Button htmlType='submit' type='primary'> Submit </Button>
          </Row>
        </Form>
      </div>
    )
  }
}

MerchantSave.propTypes = {
  merchants: PropTypes.array.isRequired,
  dispatch: PropTypes.func.isRequired
}

const mapStateToProps = state => {
  return {
    ...state
  }
}

export default connect(mapStateToProps)(MerchantSave)