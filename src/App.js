import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { Route, withRouter, Link } from 'react-router-dom'

import logo from './logo.png'
import './App.css'

import { get } from './actions/actions'

// routes
import ListMerchants from './containers'
import ViewMerchant from './containers/view'
import SaveMerchant from './containers/save'

import Card from 'antd/lib/card'

class App extends Component {
  componentDidMount () {
    this.props.dispatch(get())
  }
  
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <Link to='/'>
            <img src={logo} className="App-logo" alt="logo" />
          </Link>
        </div>
        <Card loading={!this.props.merchants.length} style={{ maxWidth: '1400px', margin: '0 auto' }}>
          <Route path='/' exact component={ListMerchants} />
          <Route path='/merchant/:id/' exact strict component={ViewMerchant} />
          <Route path='/merchant/edit/:id' exact component={SaveMerchant} />
          <Route path='/merchant/create' exact component={SaveMerchant} />
        </Card>
      </div>
    )
  }
}

App.propTypes = {
  merchants: PropTypes.array.isRequired,
  dispatch: PropTypes.func.isRequired
}

const mapStateToProps = state => {
  return {
    ...state
  }
}

export default withRouter(connect(mapStateToProps)(App))
