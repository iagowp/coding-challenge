import mock from './mock.json'

var m = 15

export const get = () => {
  return mock
}

export const create = (data) => {
  return {
    ...data,
    id: 'm' + m++,
    bids: []
  }
}

export const edit = (data) => {
  return data
}

export const deleteMerchant = (id) => {
  return id
}
