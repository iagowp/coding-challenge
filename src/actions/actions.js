import * as actionTypes from '../actiontypes/actiontypes'
import * as fakeAPI from '../fakeAPI'

export const get = () => {
  return {
    type: actionTypes.get,
    payload: fakeAPI.get()
  }
}

export const create = (data) => {
  return {
    type: actionTypes.create,
    payload: fakeAPI.create(data)
  }
}

export const edit = (data) => {
  return {
    type: actionTypes.edit,
    payload: fakeAPI.edit(data)
  }
}

export const deleteMerchant = (id) => {
  return {
    type: actionTypes.deleteMerchant,
    payload: fakeAPI.deleteMerchant(id)
  }
}
