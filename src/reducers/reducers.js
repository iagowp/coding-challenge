import * as actionTypes from '../actiontypes/actiontypes'

const initialState = {
  merchants: []
}

export default (state = initialState, action) => {
  let copyMerchants, index
  switch (action.type) {
    case actionTypes.get:
      return { merchants: action.payload }

    case actionTypes.create:
      return { merchants: [action.payload, ...state.merchants] }

    case actionTypes.edit:
      copyMerchants = [...state.merchants]
      index = copyMerchants.findIndex((merchant) => merchant.id === action.payload.id)
      copyMerchants[index] = { ...action.payload, bids: state.merchants[index].bids }
      return { merchants: copyMerchants }

    case actionTypes.deleteMerchant:
      copyMerchants = [...state.merchants]
      index = copyMerchants.findIndex((merchant) => merchant.id === action.payload)
      // probably better to do this immutably
      copyMerchants.splice(index, 1)
      return { merchants: copyMerchants }

    default:
      return state;
  }
};
