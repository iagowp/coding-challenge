# To start

`yarn` or `npm install`
`yarn start` or `npm start`


**This project was a coding challenge. What was asked:**

Develop SPA which should manage merchants. A user can interact with:
 - list of merchants (better with pagination)
 - adding merchant (redux-form is allowed but not required)
 - merchant editing
 - merchant removing
 - sorted history of bids for each merchant

Data structure example

```js
Bid {
  id: string,
  carTitle: string,
  amount: number,
  created: string
}
```

```js
Merchant {
  id: string,
  firstname: string,
  lastname: string,
  avatarUrl: string,
  email: string,
  phone: string,
  hasPremium: boolean,
  bids: Array<Bid>
}
```

# Interesting decisions:

I decided to use antd because is a library I'm familiar with, and I assumed I wouldn't be judged by my capacity to create basic components such as a button, I'd be judged by my capacity to write clean code and put together dumb components to form a screen

I decided not to store the form on redux because unless that state will be needed in different containers, keeping it as a state will make things simpler (at my current company, we store everything on redux, after doing this for sometime I currently hold the opinion that its not the best way to do things). Ofcourse there are bad sides to it, for example, if the user changes page, and then comes back while creating a user, he will lose his changes, but then again, that is kind of expected.

I wasn't sure about what my API would return, so I ended up just reproducing what the front-end passed, like a confirmation, so maybe I implemented couple stuff that could be placed on the backend at my reducer

# Next steps in case I were going to keep working on this project:

  - When creating/editing a merchant, validate fields

  - Make an actual (async ofcourse) fake API to better mock behavior. This would bring couple other things
    - Add a promise handler redux middleware, so my application knows when a request is loading, for example
    - Handle http errors
    - Change the get list action invoking from App to containers/index. Then if a user enters on edit/view page and that user is not already loaded on reducer, the application would get only that user.

  - Improve responsiveness. This table isn't really responsive, so if device screen is small, load cards for each record instead

  - Improve looks in general. I'd probably ask for some help on what should be improved, but I know it's not pretty

  - Find a way to center the switch button on save container

  - Implement Tests (ui regression, tdd, and whatever else is used on React.)
